FROM openjdk:11
#RUN addgroup -S spring && adduser -S spring -G spring
#comment
#USER spring:spring
ARG JAR_FILE=target/sample*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
